﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Vjesalo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string word = "";
        List<Label> labels = new List<Label>();
        int pokusaji = 0;
        int preostalo = 9;

        string GetRandomWord()
        {

            string path = @"D:\words.txt";
            
            string[] words = File.ReadAllLines(path);
            Random ran = new Random();
            return words[ran.Next(0, words.Length - 1)];
        }

        void MakeLabels()
        {
            word = GetRandomWord();
            char[] chars = word.ToCharArray();
            int between = 600 / chars.Length - 1;
            for (int i = 0; i < chars.Length; i++)
            {
                labels.Add(new Label());
                labels[i].Location = new Point((i * between) + 30, 90);
                labels[i].Text = "_";
                labels[i].Parent = groupBox1;
                labels[i].BringToFront();
                labels[i].CreateControl();
            }
            label1.Text = "Duljina riječi: " + (chars.Length).ToString();
            label3.Text = "Preostali pokušaji: " + (preostalo).ToString();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            MakeLabels();
        }

        private void button_rijec_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == word)
            {
                MessageBox.Show("Pobjeda!");
                Reset();
            }
            else
            {
                MessageBox.Show("Pogrešna riječ");
                pokusaji++;
                preostalo--;
                if (pokusaji == 9)
                {
                    MessageBox.Show("Izgubili ste!");
                    Reset();
                }
                label3.Text = "Preostali pokušaji: " + (preostalo).ToString();
            }
        }

        private void button_slovo_Click(object sender, EventArgs e)
        {
            char letter = textBox_slovo.Text.ToLower().ToCharArray()[0];
            if (!char.IsLetter(letter))
            {
                MessageBox.Show("Dopuštena samo slova!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
                return;
            }
            if (word.Contains(letter))
            {
                char[] letters = word.ToCharArray();
                for(int i = 0; i < letters.Length; i++)
                {
                    if(letters[i] == letter)
                    {
                        labels[i].Text = letter.ToString();
                    }
                }
                foreach(Label l in labels)
                    if (l.Text == "_") return;
                MessageBox.Show("Pobjeda!", "Čestitam");
                Reset();
            }
            else
            {
                MessageBox.Show("Riječ ne sadrži traženo slovo");
                label2.Text += letter.ToString() + ", ";
                pokusaji++;
                preostalo--;
                label3.Text = "Preostali pokušaji: " + (preostalo).ToString();
                if (pokusaji == 9)
                {
                    MessageBox.Show("Izgubili ste!");
                    Reset();
                }
            }
        }
        void Reset()
        {
            preostalo = 9;
            pokusaji = 0;
            label2.Text = "Promašena slova: ";
            textBox1.Text = "";
            GetRandomWord();
            MakeLabels();
        }
    }
}
